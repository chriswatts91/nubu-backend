'use strict'

const Promise = require('bluebird')
const requestPromise = require('request-promise')

const Genre = require('../genre')

class Events {

  /**
   * Constructor
   *
   * @param {array} options
   */
  constructor (options) {
    const self = this
    const allowedSorts = ['time', 'distance', 'venue', 'popularity']
    let timeNow = (new Date().getTime() / 1000).toFixed()
    let sinceTime = timeNow - (10 * 3600)

    self.latitude = options.lat || null
    self.longitude = options.lng || null
    self.distance = options.distance || 2000
    self.accessToken = options.accessToken ? options.accessToken : (process.env.FB_ACCESS_TOKEN && process.env.FB_ACCESS_TOKEN !== '' ? process.env.FB_ACCESS_TOKEN : null)
    self.query = options.query ? encodeURIComponent(options.query) : ''
    self.queries = ['bars', 'clubs', 'entertainment', '']
    self.sort = options.sort ? (allowedSorts.indexOf(options.sort.toLowerCase()) > -1 ? options.sort.toLowerCase() : null) : null
    self.version = options.version ? options.version : 'v2.7'
    self.since = options.since || sinceTime
    self.until = options.until || null
    self.eventId = options.eventId || null
  }

  calculateStarttimeDifference (currentTime, dataString) {
    return (new Date(dataString).getTime() - (currentTime * 1000)) / 1000
  }

  compareVenue (a, b) {
    if (a.venue.name < b.venue.name) return -1
    if (a.venue.name > b.venue.name) return 1
    return 0
  }

  compareTimeFromNow (a, b) {
    if (a.timeFromNow < b.timeFromNow) return -1
    if (a.timeFromNow > b.timeFromNow) return 1
    return 0
  }

  compareDistance (a, b) {
    var aEventDistInt = parseInt(a.distance, 10)
    var bEventDistInt = parseInt(b.distance, 10)
    if (aEventDistInt < bEventDistInt) return -1
    if (aEventDistInt > bEventDistInt) return 1
    return 0
  };

  comparePopularity (a, b) {
    if ((a.stats.attending + (a.stats.maybe / 2)) < (b.stats.attending + (b.stats.maybe / 2))) return 1
    if ((a.stats.attending + (a.stats.maybe / 2)) > (b.stats.attending + (b.stats.maybe / 2))) return -1
    return 0
  }

  /**
   * Calculate distance between two coordinates
   *
   * @param {double} coords1 - Coordinate 1
   * @param {double} coords2  Coordinate 2
   * @param {bool} isMiles - Return value in miles?
   */
  haversineDistance (coords1, coords2, isMiles) {
    // coordinate is [latitude, longitude]
    function toRad (x) {
      return x * Math.PI / 180
    }

    const lon1 = coords1[1]
    const lat1 = coords1[0]

    const lon2 = coords2[1]
    const lat2 = coords2[0]

    const R = 6371 // km

    const x1 = lat2 - lat1
    const dLat = toRad(x1)
    const x2 = lon2 - lon1
    const dLon = toRad(x2)
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    let d = R * c

    if (isMiles) d /= 1.60934

    return d
  }

  /**
   * Check if venue id has been added
   *
   * @param id
   * @param ids
   * @returns {boolean}
   */
  venueIdExists (id, ids) {
    let venueFound = false
    if (ids && ids.length) {
      ids.map((idsArray) => {
        if (idsArray.indexOf(id) > -1) {
          venueFound = true
        }
      })
    }
    return venueFound
  }

  /**
   * Check if event has already been added
   * @param id
   * @param events
   * @returns {boolean}
   */
  eventAlreadyExists (id, events) {
    let eventFound = false
    if (events && events.length) {
      events.map((event) => {
        if (event.id === id) {
          eventFound = true
        }
      })
    }
    return eventFound
  }

  /**
   * Search Facebook for events
   *
   * @returns {promise} Promise of events
   */
  search () {
    const self = this
    let error

    return new Promise(function (resolve, reject) {
      if (!self.latitude || !self.longitude) {
        error = {
          'message': 'Please specify the lat and lng parameters',
          'code': 1
        }
        reject(error)
      } else if (!self.accessToken) {
        error = {
          'message': 'Please specify an Access Token, either as environment variable or as accessToken parameter',
          'code': 2
        }
        reject(error)
      } else {
        const idLimit = 50 // FB only allows 50 ids per /?ids= call
        const currentTimestamp = (new Date().getTime() / 1000).toFixed()
        let venuesCount = 0
        let venuesWithEvents = 0
        let eventsCount = 0
        let placePromiseRequests = []

        self.queries.map((query) => {
          let placeUrl = 'https://graph.facebook.com/' + self.version + '/search' +
            '?type=place' +
            '&q=' + query +
            '&center=' + self.latitude + ',' + self.longitude +
            '&distance=' + self.distance +
            '&limit=1000' +
            '&fields=id' +
            '&access_token=' + self.accessToken

          placePromiseRequests.push(requestPromise.get(placeUrl))
        })

        // Get places as specified
        Promise.all(placePromiseRequests).then(function (results) {
          let ids = []

          results.forEach(function (resStr) {
            let data = JSON.parse(resStr).data
            let tempArray = []

            // Create array of 50 places each
            data.forEach(function (idObj) {
              if (!self.venueIdExists(idObj.id, ids)) {
                tempArray.push(idObj.id)
                venuesCount++
              }

              if (tempArray.length >= idLimit) {
                ids.push(tempArray)
                tempArray = []
              }
            })

            // Push the remaining places
            if (tempArray.length > 0) {
              // Plonk Bar
              tempArray.push('1591505004470818')
              ids.push(tempArray)
            }
          })

          return ids
        }).then(function (ids) {
          let urls = []

          // Create a Graph API request array (promisified)
          ids.forEach(function (idArray, index, arr) {
            let eventsFields = [
              'id',
              'type',
              'name',
              'cover.fields(id,source)',
              'picture.type(large)',
              'description',
              'start_time',
              'end_time',
              'attending_count',
              'declined_count',
              'maybe_count',
              'noreply_count'
            ]

            let fields = [
              'id',
              'name',
              'about',
              'emails',
              'cover.fields(id,source)',
              'picture.type(large)',
              'location',
              'events.fields(' + eventsFields.join(',') + ')'
            ]

            let eventsUrl = 'https://graph.facebook.com/' + self.version + '/' +
              '?ids=' + idArray.join(',') +
              '&access_token=' + self.accessToken +
              '&fields=' + fields.join(',') +
              '.since(' + self.since + ')'
            if (self.until) {
              eventsUrl += '.until(' + self.until + ')'
            }
            urls.push(requestPromise.get(eventsUrl))
          })

          return urls
        }).then(function (promisifiedRequests) {
          // Run Graph API requests in parallel
          return Promise.all(promisifiedRequests)
        }).then(function (results) {
          let events = []

          // Handle results
          results.forEach(function (resStr, index, arr) {
            let resObj = JSON.parse(resStr)
            Object.getOwnPropertyNames(resObj).forEach(function (venueId, index, array) {
              let venue = resObj[venueId]
              if (venue.events && venue.events.data.length > 0) {
                venuesWithEvents++
                venue.events.data.forEach(function (event, index, array) {
                  let eventResultObj = {}
                  eventResultObj.id = event.id
                  eventResultObj.name = event.name
                  eventResultObj.type = event.type
                  eventResultObj.coverPicture = (event.cover ? event.cover.source : null)
                  eventResultObj.profilePicture = (event.picture ? event.picture.data.url : null)
                  eventResultObj.description = (event.description ? event.description : null)
                  eventResultObj.genre = Genre.getGenres(event)
                  eventResultObj.distance = (venue.location ? (self.haversineDistance([venue.location.latitude, venue.location.longitude], [self.latitude, self.longitude], false) * 1000).toFixed() : null)
                  eventResultObj.startTime = (event.start_time ? event.start_time : null)
                  eventResultObj.endTime = (event.end_time ? event.end_time : null)
                  eventResultObj.timeFromNow = self.calculateStarttimeDifference(currentTimestamp, event.start_time)
                  eventResultObj.stats = {
                    attending: event.attending_count,
                    declined: event.declined_count,
                    maybe: event.maybe_count,
                    noreply: event.noreply_count
                  }
                  eventResultObj.venue = {}
                  eventResultObj.venue.id = venueId
                  eventResultObj.venue.name = venue.name
                  eventResultObj.venue.about = (venue.about ? venue.about : null)
                  eventResultObj.venue.emails = (venue.emails ? venue.emails : null)
                  eventResultObj.venue.coverPicture = (venue.cover ? venue.cover.source : null)
                  eventResultObj.venue.profilePicture = (venue.picture ? venue.picture.data.url : null)
                  eventResultObj.venue.location = (venue.location ? venue.location : null)

                  var startTime = new Date(event.start_time)
                  var startTimeHour = startTime.getHours()
                  if (startTimeHour <= 7 || startTimeHour >= 17) {
                    if (!self.eventAlreadyExists(event.id, events)) {
                      events.push(eventResultObj)
                      eventsCount++
                    }
                  }
                })
              }
            })
          })

          // Sort if requested
          if (self.sort) {
            switch (self.sort) {
              case 'time':
                events.sort(self.compareTimeFromNow)
                break
              case 'distance':
                events.sort(self.compareDistance)
                break
              case 'venue':
                events.sort(self.compareVenue)
                break
              case 'popularity':
                events.sort(self.comparePopularity)
                break
              default:
                break
            }
          }

          // Produce result object
          resolve({
            events: events,
            metadata: {venues: venuesCount, venuesWithEvents: venuesWithEvents, events: eventsCount}
          })
        }).catch(function (e) {
          var error = {
            'message': e,
            'code': -1
          }
          console.error(JSON.stringify(error))
          reject(error)
        })
      }
    })
  }

  /**
   * Get Event
   *
   * @returns {promise} Promise of the event
   */
  get () {
    const self = this
    let error

    return new Promise(function (resolve, reject) {
      if (!self.eventId) {
        error = {
          'message': 'Please specify the event',
          'code': 1
        }
        reject(error)
      } else if (!self.accessToken) {
        error = {
          'message': 'Please specify an Access Token, either as environment variable or as accessToken parameter',
          'code': 2
        }
        reject(error)
      } else {
        let fields = [
          'id',
          'type',
          'name',
          'cover.fields(id,source)',
          'description',
          'start_time',
          'end_time',
          'attending_count',
          'declined_count',
          'maybe_count',
          'noreply_count',
          'picture.type(large)',
          'place'
        ]

        let eventUrl = 'https://graph.facebook.com/' + self.version + '/' +
          self.eventId +
          '?access_token=' + self.accessToken +
          '&fields=' + fields.join(',')

        let eventPromiseRequests = requestPromise.get(eventUrl)

        Promise.resolve(eventPromiseRequests).then(function (result) {
          let event = JSON.parse(result)
          let eventResultObj = {}
          eventResultObj.id = event.id
          eventResultObj.name = event.name
          eventResultObj.type = event.type
          eventResultObj.coverPicture = (event.cover ? event.cover.source : null)
          eventResultObj.profilePicture = (event.picture ? event.picture.data.url : null)
          eventResultObj.description = (event.description ? event.description : null)
          eventResultObj.genre = Genre.getGenres(event)
          eventResultObj.startTime = (event.start_time ? event.start_time : null)
          eventResultObj.endTime = (event.end_time ? event.end_time : null)
          eventResultObj.stats = {
            attending: event.attending_count,
            declined: event.declined_count,
            maybe: event.maybe_count,
            noreply: event.noreply_count
          }
          eventResultObj.venue = {}
          eventResultObj.venue.id = event.place.id
          eventResultObj.venue.name = event.place.name
          eventResultObj.venue.location = (event.place.location ? event.place.location : null)

          resolve({event: eventResultObj})
        }).catch(function (e) {
          var error = {
            'message': e,
            'code': -1
          }
          console.error(JSON.stringify(error))
          reject(error)
        })
      }
    })
  }
}

module.exports = Events
