/* eslint no-unused-vars: "error" */
/* global should */
/* eslint-env mocha */

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const should = chai.should()
const Promise = require('bluebird')
const path = require('path')
const fs = require('fs')
const Ajv = require('ajv')
const ajv = new Ajv()

const Events = require('./events')

chai.use(chaiAsPromised)
chai.config.includeStack = false

describe('Events', function () {
  const accessToken = '396190023730569|zbFTRSQfGNPZlgYHiupclDAzkzY'

  describe('Basic unit tests', function () {
    it('should return a list of events for a popular coordinate (Leeds, UK)', function (done) {
      // Set timeout
      this.timeout(10000)

      const events = new Events({
        'lat': 53.800755,
        'lng': -1.549077,
        'distance': 1000,
        'accessToken': accessToken
      })

      events.search().should.be.fulfilled.and.notify(done)
    })
    it('should return an error if no Access Token is present', function (done) {
      const events = new Events({
        'lat': 53.800755,
        'lng': -1.549077,
        'distance': 1000
      })

      events.search().should.be.rejectedWith(1).and.notify(done)
    })
    it('should return an error if a partial coordinate is used', function (done) {
      const events = new Events({
        'lat': 53.800755,
        'accessToken': accessToken
      })

      events.search().should.be.rejectedWith(2).and.notify(done)
    })
    it('should return a valid JSON schema', function (done) {
      // Set timeout
      this.timeout(10000)

      const schema = JSON.parse(fs.readFileSync(path.join(__dirname, './', 'events-response.schema.json'), 'utf8'))

      const events = new Events({
        'lat': 53.800755,
        'lng': -1.549077,
        'distance': 1000,
        'accessToken': accessToken
      })

      events.search().then(function (events) {
        const validate = ajv.compile(schema)
        const valid = validate(events)
        return new Promise(function (resolve, reject) {
          if (!valid) {
            console.error(validate.errors)
            reject(validate.errors)
          } else {
            resolve(valid)
          }
        })
      }).should.be.fulfilled.and.notify(done)
    })
  })
})
