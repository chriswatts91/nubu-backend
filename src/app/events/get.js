'use strict'

const Events = require('./events')

module.exports.endpoint = (event, context, callback) => {
  let eventId = event.pathParameters.id
  let options = {}
  if (eventId) {
    options.eventId = eventId
  }

  // Instantiate event
  const events = new Events(options)

  events.get().then(function (events) {
    callback(null, {
      statusCode: 200,
      body: JSON.stringify(events)
    })
  }).catch(function (error) {
    callback(null, {
      statusCode: 500,
      body: JSON.stringify(error)
    })
  })
}
