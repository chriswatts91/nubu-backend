'use strict'

const Events = require('./events')

module.exports.endpoint = (event, context, callback) => {
  let data = event.queryStringParameters
  let options = {}
  if (data.lat) {
    options.lat = data.lat
  }
  if (data.lng) {
    options.lng = data.lng
  }
  if (data.distance) {
    options.distance = data.distance
  }
  if (data.query) {
    options.query = data.query
  }
  if (data.sort) {
    options.sort = data.sort
  }
  if (data.version) {
    options.version = data.version
  }
  if (data.since) {
    options.since = data.since
  }
  if (data.until) {
    options.until = data.until
  }

  // Instantiate event search
  const events = new Events(options)

  events.search().then(function (events) {
    callback(null, {
      statusCode: 200,
      body: JSON.stringify(events)
    })
  }).catch(function (error) {
    callback(null, {
      statusCode: 500,
      body: JSON.stringify(error)
    })
  })
}
