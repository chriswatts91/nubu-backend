'use strict'

const genresJson = require('./genres.json')

class Genre {
  static getGenres (event) {
    const genres = genresJson.genres
    let genresFound = []

    // Loop through the predefined genres
    for (let key in genres) {
      const description = String(event.description).toLowerCase()
      const genre = genres[key]

      // Search the event description hints of the genre
      if (description.search(genre.keywords) > -1) {
        // If found, push it
        const genreFound = {
          'name': genre.name,
          'subGenres': []
        }
        genresFound.push(genreFound)
      }

      // Loop through the predefined subgenres of the current genre
      for (let key in genre.subGenres) {
        const subGenre = genre.subGenres[key]

        // Search the event description hints of the subgenre
        if (description.search(subGenre.keywords) > -1) {
          // Has the genre already been added to the event
          if (genresFound.findIndex(x => x.name === genre.name) === -1) {
            const genreFound = {
              'name': genre.name,
              'subGenres': []
            }
            // If not add it
            genresFound.push(genreFound)
          }

          // Get the index for the genre
          const index = genresFound.findIndex(x => x.name === genre.name)
          const subGenreFound = {
            'name': subGenre.name
          }

          // Has the subgenre already been added to the event
          if (genresFound[index].subGenres.findIndex(x => x.name === genre.name) === -1) {
            // If not add it
            genresFound[index].subGenres.push(subGenreFound)
          }
        }
      }
    }

    return genresFound
  }
}

module.exports = Genre
